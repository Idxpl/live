const playlistData = [
    {
        title: "Bein Sports 1",
        description: "SPORTS",
        image: "https://www.fetchtv.com.au/images/components/rotating-header/packages/special-interest/bein-sports-tile-bg.png",
        url: "https://av-ch-cdn.mncnow.id/live/eds/BEIN01/sa_dash_vmx/BEIN01.mpd",
        drmOptions: {
            keyId: "57d2ac9210cfbca3596cc679a01c8b29",
            key: "d5e35c0f39c76adf24853d7ea18c71e7"
        }
    },
    {
        title: "Bein Sports 2",
        description: "SPORTS",
        image: "https://www.fetchtv.com.au/images/components/rotating-header/packages/special-interest/bein-sports-tile-bg.png",
        url: "https://av-ch-cdn.mncnow.id/live/eds/BEIN02/sa_dash_vmx/BEIN02.mpd",
        drmOptions: {
            keyId: "57d2ac9210cfbca3596cc679a01c8b29",
            key: "d5e35c0f39c76adf24853d7ea18c71e7"
        }
    },
    {
        title: "Bein Sports 3",
        description: "SPORTS",
        image: "https://www.fetchtv.com.au/images/components/rotating-header/packages/special-interest/bein-sports-tile-bg.png",
        url: "https://av-ch-cdn.mncnow.id/live/eds/BEIN03/sa_dash_vmx/BEIN03.mpd",
        drmOptions: {
            keyId: "57d2ac9210cfbca3596cc679a01c8b29",
            key: "d5e35c0f39c76adf24853d7ea18c71e7"
        }
    },
    {
        title: "Bein Sports 4",
        description: "SPORTS",
        image: "https://www.fetchtv.com.au/images/components/rotating-header/packages/special-interest/bein-sports-tile-bg.png",
        url: "https://av-ch-cdn.mncnow.id/live/eds/BEIN04/sa_dash_vmx/BEIN04.mpd",
        drmOptions: {
            keyId: "57d2ac9210cfbca3596cc679a01c8b29",
            key: "d5e35c0f39c76adf24853d7ea18c71e7"
        }
    },
    {
        title: "Bein Sports 5",
        description: "SPORTS",
        image: "https://www.fetchtv.com.au/images/components/rotating-header/packages/special-interest/bein-sports-tile-bg.png",
        url: "https://av-ch-cdn.mncnow.id/live/eds/BEIN05/sa_dash_vmx/BEIN05.mpd",
        drmOptions: {
            keyId: "57d2ac9210cfbca3596cc679a01c8b29",
            key: "d5e35c0f39c76adf24853d7ea18c71e7"
        }
    },
    {
        title: "Sportstar",
        description: "SPORTS",
        image: "https://img.inews.co.id/media/822/files/inews_new/2021/05/24/sportstars.jpeg",
        url: "https://av-ch-cdn.mncnow.id/live/eds/MNCSports-HD/sa_dash_vmx/MNCSports-HD.mpd",
        drmOptions: {
            keyId: "531c6d50e3e9f9ba66446f624f492289",
            key: "d769d9ae238bdd424f8bcdcdc9a3801f"
        }
    },
    {
        title: "Sportstar2",
        description: "SPORTS",
        image: "https://img.inews.co.id/media/822/files/inews_new/2021/05/24/sportstars.jpeg",
        url: "https://av-ch-cdn.mncnow.id/live/eds/MNCSports2-HD/sa_dash_vmx/MNCSports2-HD.mpd",
        drmOptions: {
            keyId: "45fec91ce1f19b6b1f31d69dcfaaf6cd",
            key: "843e228ab109e9aa6c4822ee4ad05d7d"
        }
    },
    {
        title: "SPOTV",
        description: "SPORTS",
        image: "https://www.sportcal.com/wp-content/uploads/sites/32/2022/06/file_7408e006-5492-4b54-aa54-581581b2d87b.png",
        url: "https://av-ch-cdn.mncnow.id/live/eds/SPOTV-HD/sa_dash_vmx/SPOTV-HD.mpd",
        drmOptions: {
            keyId: "57d2ac9210cfbca3596cc679a01c8b29",
            key: "d5e35c0f39c76adf24853d7ea18c71e7"
        }
    },
    {
        title: "SPOTV2",
        description: "SPORTS",
        image: "https://www.sportcal.com/wp-content/uploads/sites/32/2022/06/file_7408e006-5492-4b54-aa54-581581b2d87b.png",
        url: "https://av-ch-cdn.mncnow.id/live/eds/SPOTV2-HD/sa_dash_vmx/SPOTV2-HD.mpd",
        drmOptions: {
            keyId: "57d2ac9210cfbca3596cc679a01c8b29",
            key: "d5e35c0f39c76adf24853d7ea18c71e7"
        }
    },
    {
        title: "Soccer",
        description: "SPORTS",
        image: "https://blog.visionplus.id/wp-content/uploads/2024/01/jadwal-program-soccer-channel-1536x864.jpg",
        url: "https://av-ch-cdn.mncnow.id/live/eds/soccerchannel-test/sa_dash_vmx/soccerchannel-test.mpd",
        drmOptions: {
            keyId: "4d38060bf41b3c29df0ec950ece6b5da",
            key: "7ee9506b13480491d79b71c062ab5366"
        }
    },
    {
        title: "Viaplay",
        description: "SPORTS",
        image: "https://ik.imagekit.io/tp/20220201-viaplay-logo.png?tr=w-333,h-190",
        url: "https://stream.spesialwar007.workers.dev/https://live-dash-cdn7-vp.cdn.viaplay.tv/vp/vssp/vssp.isml/index.mpd",
        drmOptions: {
            keyId: "d8d0506523d95fc082d6caef60e84979",
            key: "69992fd2d7b9211d41ecc2243803e87c"
        }
    },
    {
        title: "TNT Sports 1",
        description: "SPORTS",
        image: "https://www.sportspromedia.com/wp-content/uploads/2023/08/tnt-sports-logo-transparent.png",
        url: "https://live.ll.ww.aiv-cdn.net/OTTB/lhr-nitro/live/clients/dash/enc/wf8usag51e/out/v1/bd3b0c314fff4bb1ab4693358f3cd2d3/cenc.mpd",
        drmOptions: {
            keyId: "ae26845bd33038a9c0774a0981007294",
            key: "63ac662dde310cfb4cc6f9b43b34196d"
        }
    },
    {
        title: "TNT Sports 2",
        description: "SPORTS",
        image: "https://www.sportspromedia.com/wp-content/uploads/2023/08/tnt-sports-logo-transparent.png",
        url: "https://live.ll.ww.aiv-cdn.net/OTTB/lhr-nitro/live/clients/dash/enc/f0qvkrra8j/out/v1/f8fa17f087564f51aa4d5c700be43ec4/cenc.mpd",
        drmOptions: {
            keyId: "6d1708b185c6c4d7b37600520c7cc93c",
            key: "1aace05f58d8edef9697fd52cb09f441"
        }
    },
    {
        title: "TNT Sports 3",
        description: "SPORTS",
        image: "https://www.sportspromedia.com/wp-content/uploads/2023/08/tnt-sports-logo-transparent.png",
        url: "https://live.ll.ww.aiv-cdn.net/OTTB/lhr-nitro/live/clients/dash/enc/lsdasbvglv/out/v1/bb548a3626cd4708afbb94a58d71dce9/cenc.mpd",
        drmOptions: {
            keyId: "4e993aa8c1f295f8b94e8e9e6f6d0bfe",
            key: "86a1ed6e96caab8eb1009fe530d2cf4f"
        }
    },
        {
        title: "TNT Sports 4",
        description: "SPORTS",
        image: "https://www.sportspromedia.com/wp-content/uploads/2023/08/tnt-sports-logo-transparent.png",
        url: "https://live.ll.ww.aiv-cdn.net/OTTB/lhr-nitro/live/clients/dash/enc/i2pcjr4pe5/out/v1/912e9db56d75403b8a9ac0a719110f36/cenc.mpd",
        drmOptions: {
            keyId: "e31a5a81caff5d07ea2411a571fc2e59",
            key: "96c5ef69479732ae734f962748c19729"
        }
    },
        {
        title: "TNT Sports 5",
        description: "SPORTS",
        image: "https://www.sportspromedia.com/wp-content/uploads/2023/08/tnt-sports-logo-transparent.png",
        url: "https://live.ll.ww.aiv-cdn.net/OTTB/lhr-nitro/live/clients/dash/enc/gesdwrdncn/out/v1/79e752f1eccd4e18b6a8904a0bc01f2d/cenc.mpd",
        drmOptions: {
            keyId: "60c0d9b41475e01db4ffb91ed557fbcc",
            key: "36ee40e58948ca15e3caba8d47b8f34b"
        }
    },
        {
        title: "Sky Sport1",
        description: "SPORTS",
        image: "https://www.kindpng.com/picc/m/27-278109_sky-sport-nz-logo-sky-sport-1-nz.png",
        url: "https://linear-s.media.skyone.co.nz/sky-sport-1.mpd",
        drmOptions: {
            keyId: "96be5ca21f087c8cb1d7630457e20000",
            key: "53cb495864a442f4b7e7df8b540e035d"
        }
    },
        {
        title: "Sky Sport2",
        description: "SPORTS",
        image: "https://www.kindpng.com/picc/m/27-278109_sky-sport-nz-logo-sky-sport-1-nz.png",
        url: "https://linear-s.media.skyone.co.nz/sky-sport-2.mpd",
        drmOptions: {
            keyId: "ef909acce1f53f5db2a1cdafd2e90000",
            key: "aba1269d685f474fbddf97ce2b45c725"
        }
    },
        {
        title: "Sky Sport3",
        description: "SPORTS",
        image: "https://www.kindpng.com/picc/m/27-278109_sky-sport-nz-logo-sky-sport-1-nz.png",
        url: "https://linear-s.media.skyone.co.nz/sky-sport-3.mpd",
        drmOptions: {
            keyId: "79553de34321f11972a72b1e34620000",
            key: "6f134b10fed345c8be29ab4b318ce502"
        }
    },
        {
        title: "Sky Sport5",
        description: "SPORTS",
        image: "https://www.kindpng.com/picc/m/27-278109_sky-sport-nz-logo-sky-sport-1-nz.png",
        url: "https://linear-s.media.skyone.co.nz/sky-sport-5.mpd",
        drmOptions: {
            keyId: "5492033ac2dc11141cd5c1d1d7a80000",
            key: "ed8219b7064849d385d26151c90bd306"
        }
    },
        {
        title: "Sky Sport6",
        description: "SPORTS",
        image: "https://www.kindpng.com/picc/m/27-278109_sky-sport-nz-logo-sky-sport-1-nz.png",
        url: "https://linear-s.media.skyone.co.nz/sky-sport-6.mpd",
        drmOptions: {
            keyId: "a1c36ae483b44fc78025fae101030000",
            key: "e4ea0adb660139309ab060cfed1a9d3a"
        }
    },
        {
        title: "Sky Sport7",
        description: "SPORTS",
        image: "https://www.kindpng.com/picc/m/27-278109_sky-sport-nz-logo-sky-sport-1-nz.png",
        url: "https://linear-s.media.skyone.co.nz/sky-sport-7.mpd",
        drmOptions: {
            keyId: "e2fcb4139149486facbfa3192ae00000",
            key: "f72cfa3a2c1b1f5575421e835ca2a59b"
        }
    },
        {
        title: "Sky Sport8",
        description: "SPORTS",
        image: "https://www.kindpng.com/picc/m/27-278109_sky-sport-nz-logo-sky-sport-1-nz.png",
        url: "https://linear-s.media.skyone.co.nz/sky-sport-8.mpd",
        drmOptions: {
            keyId: "c8997f53b7494072bdc5c8d1c5f30000",
            key: "bde3d1054eea001d0693bb846e124e75"
        }
    },
        {
        title: "Sky Sport9",
        description: "SPORTS",
        image: "https://www.kindpng.com/picc/m/27-278109_sky-sport-nz-logo-sky-sport-1-nz.png",
        url: "https://linear-s.media.skyone.co.nz/sky-sport-9.mpd",
        drmOptions: {
            keyId: "ac3615431ccb4a14aedc75dbea7c0000",
            key: "b50ae9ebd7274c90244989fed8061e7b"
        }
    },
        {
        title: "Sky Sport Select",
        description: "SPORTS",
        image: "https://www.kindpng.com/picc/m/27-278109_sky-sport-nz-logo-sky-sport-1-nz.png",
        url: "https://linear-s.media.skyone.co.nz/sky-sport-select.mpd",
        drmOptions: {
            keyId: "aea66cdd9fea4096a81c9640c22f0000",
            key: "c6a0d6507a25539719272484c7764742"
        }
    },
        {
        title: "Sky Sport Popup2",
        description: "SPORTS",
        image: "https://www.kindpng.com/picc/m/27-278109_sky-sport-nz-logo-sky-sport-1-nz.png",
        url: "https://linear-s.media.skyone.co.nz/sky-sport-popup-2.mpd",
        drmOptions: {
            keyId: "55ecc57cf67448b485bd8638d2c70000",
            key: "b63406a5c61d87841947ec4664e193b3"
        }
    },
        {
        title: "dazn de",
        description: "SPORTS",
        image: "https://www.kindpng.com/picc/m/27-278109_sky-sport-nz-logo-sky-sport-1-nz.png",
        url: "https://dcf-fs-live-dazn-cdn.dazn.com/dash/dazn-linear-017/stream.mpd",
        drmOptions: {
            keyId: "8ab47741930c476780515f9a00decb0a",
            key: "7ab4b9ae5a48aa526e511a913b832769"
        }
    },
        {
        title: "RCTI+",
        description: "SPORTS",
        image: "https://www.kindpng.com/picc/m/27-278109_sky-sport-nz-logo-sky-sport-1-nz.png",
        url: "https://1s1.rctiplus.id/mnctv2023.m3u8",
        drmOptions: {
            keyId: "",
            key: ""
        }
    },
        {
        title: "001",
        description: "SPORTS",
        image: "",
        url: "",
        drmOptions: {
            keyId: "",
            key: ""
        }
    }
];

// Ekspor variabel untuk digunakan di file lain
export default playlistData;