export default function handler(req, res) {
  // Tambahkan header CORS
  res.setHeader('Access-Control-Allow-Origin', '*'); // Ganti '*' dengan domain spesifik jika perlu
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Content-Type, Authorization'
  );

  // Tangani preflight request
  if (req.method === 'OPTIONS') {
    res.status(200).end();
    return;
  }

  // Contoh respons dari API
  res.status(200).json({ message: 'CORS diaktifkan!' });
}
